<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<?php $base_path = base_path() . path_to_theme(); ?>

<body>
  <header>
    <div class="center">
      <div id="logo">
        <?php print '<a href="'. check_url($base_path) .'" title="'. $site_title .'">';
              print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
            print $site_html .'</a>';?>
      </div>
      <div id="header-area">
        <?php echo $header; ?> 
      </div>
    </div>
  </header>
  <nav id="main-nav">
    <div class="center">
      <?php echo $mainmenu;?>
    </div>
  </nav>
  <section id="slider">
    <?php echo $slider; ?>
  </section>
  <main id="content">
    <div class="center clearfix">
      <div class="maincontent">
        <?php print $breadcrumb ?>
        <!-- img width="700" height="4" src="/sites/default/files/Polosa.gif"/ style="padding: 0px; margin: 0px;" -->
        <?php if(drupal_is_front_page()): ?><h1 class="title"><?php print $title ?></h1><?php endif; ?>
        <div class="tabs"><?php print $tabs ?></div>
        <?php if ($show_messages) { print $messages; } ?>
        <?php print $help ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
      </div>
    </div>
  </main>

  <footer>
    <div class="center clearfix">
      <?php print $footer_message ?>
      <?php print $footer ?>
      <div id="footer-end" class="cols">
        <div id="copyrights" class="col">
          Copyright (c) 2009-<?php $a = getdate(); echo $a['year']; ?> ЗАО «НПФ «ЭНЕРГОСОЮЗ»
        </div>
        <div id="ss-footer" class="col">
          <!--Rating@Mail.ru counter-->
          <script language="javascript"><!--
          d=document;var a='';a+=';r='+escape(d.referrer);js=10;//--></script>
          <script language="javascript1.1"><!--
          a+=';j='+navigator.javaEnabled();js=11;//--></script>
          <script language="javascript1.2"><!--
          s=screen;a+=';s='+s.width+'*'+s.height;
          a+=';d='+(s.colorDepth?s.colorDepth:s.pixelDepth);js=12;//--></script>
          <script language="javascript1.3"><!--
          js=13;//--></script><script language="javascript" type="text/javascript"><!--
          d.write('<a href="http://top.mail.ru/jump?from=1821056" target="_top">'+
          '<img src="http://d9.cc.bb.a1.top.mail.ru/counter?id=1821056;t=210;js='+js+
          a+';rand='+Math.random()+'" alt="Рейтинг@Mail.ru" border="0" '+
          'height="31" width="88"><\/a>');if(11<js)d.write('<'+'!-- ');//--></script>
          <noscript><a target="_top" href="http://top.mail.ru/jump?from=1821056">
          <img src="http://d9.cc.bb.a1.top.mail.ru/counter?js=na;id=1821056;t=210" 
          height="31" width="88" border="0" alt="Рейтинг@Mail.ru"></a></noscript>
          <script language="javascript" type="text/javascript"><!--
          if(11<js)d.write('--'+'>');//--></script>
          <!--// Rating@Mail.ru counter-->

          <!-- HotLog -->
          <script type="text/javascript" language="javascript">
          hotlog_js="1.0"; hotlog_r=""+Math.random()+"&s=2127439&im=605&r="+
          escape(document.referrer)+"&pg="+escape(window.location.href);
          </script>
          <script type="text/javascript" language="javascript1.1">
          hotlog_js="1.1"; hotlog_r+="&j="+(navigator.javaEnabled()?"Y":"N");
          </script>
          <script type="text/javascript" language="javascript1.2">
          hotlog_js="1.2"; hotlog_r+="&wh="+screen.width+"x"+screen.height+"&px="+
          (((navigator.appName.substring(0,3)=="Mic"))?screen.colorDepth:screen.pixelDepth);
          </script>
          <script type="text/javascript" language="javascript1.3">
          hotlog_js="1.3";
          </script>
          <script type="text/javascript" language="javascript">
          hotlog_r+="&js="+hotlog_js;
          document.write('<a href="http://click.hotlog.ru/?2127439" target="_blank"><img '+
          'src="http://hit37.hotlog.ru/cgi-bin/hotlog/count?'+
          hotlog_r+'" border="0" width="88" height="31" alt="HotLog"><\/a>');
          </script>
          <noscript>
          <a href="http://click.hotlog.ru/?2127439" target="_blank"><img
          src="http://hit37.hotlog.ru/cgi-bin/hotlog/count?s=2127439&im=605" border="0"
          width="88" height="31" alt="HotLog"></a>
          </noscript>
          <!-- /HotLog -->

          <!-- Yandex.Metrika informer -->
          <a href="https://metrika.yandex.ru/stat/?id=22285184&amp;from=informer"
          target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/22285184/3_0_FFFFFFFF_EFEFEFFF_0_pageviews"
          style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="22285184" data-lang="ru" /></a>
          <!-- /Yandex.Metrika informer -->

          <!-- Yandex.Metrika counter -->
          <script type="text/javascript">
              (function (d, w, c) {
                  (w[c] = w[c] || []).push(function() {
                      try {
                          w.yaCounter22285184 = new Ya.Metrika({
                              id:22285184,
                              clickmap:true,
                              trackLinks:true,
                              accurateTrackBounce:true,
                              webvisor:true
                          });
                      } catch(e) { }
                  });

                  var n = d.getElementsByTagName("script")[0],
                      s = d.createElement("script"),
                      f = function () { n.parentNode.insertBefore(s, n); };
                  s.type = "text/javascript";
                  s.async = true;
                  s.src = "https://mc.yandex.ru/metrika/watch.js";

                  if (w.opera == "[object Opera]") {
                      d.addEventListener("DOMContentLoaded", f, false);
                  } else { f(); }
              })(document, window, "yandex_metrika_callbacks");
          </script>
          <noscript><div><img src="https://mc.yandex.ru/watch/22285184" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
          <!-- /Yandex.Metrika counter -->
        </div>
      </div>
  </div>
</footer>

<?php print $closure ?>
</body>
</html>
 
