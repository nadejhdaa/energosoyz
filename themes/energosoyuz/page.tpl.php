﻿<?php
// $Id: page.tpl.php,v 1.28 2008/01/24 09:42:52 goba Exp $
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<?php $base_path = base_path() . path_to_theme(); ?>

<body height="100%">

<script type="text/javascript">

$(document).ready(function()
{
    	$(".expanded > a").click(function(){
/*    	$(this).toggleClass("active");*/
		$(this).next(".menu").slideToggle("slow");
		/*$(this).next(".menu").toggleClass("active");*/
		return false;
    	});
    	
	$(".expanded > ul").slideToggle("fast");
});

</script>


<a name="toptop"></a>

    <table cellspacing="0" cellpadding="0" width="1100" align="center" height="100%">
      <tr valign="top" height="100%">
        <td class="mainbar-left-line" height="100%">
        <td height="100%">
        
        

<table style="max-width:1000px; width:1000px;" align="center" cellspacing="0" cellpadding="0"  height="100%">

 <tr style="height:180px;">
  <td style="padding: 0px; margin: 0px;">
    <img src="<?php print $base_path?>/logo.jpg" border="0" align="left" usemap="#karta1">
    <map name="karta1">
      <area href="/" shape="rect" coords="0,0,430,130">
    </map>

 <tr style="height: 25px;">
  <td style="border-top: 0px;">
    <table id="header-region" align="left" style="top: 0px; width: 100%;" cellspacing="0" cellpadding="0">
    <tr style="top: 0px;">
      <td style="top: 0px;"><?php print $header ?>
    </table>
 <tr height="100%">
  <td height="100%">
  
  <table style="width: 100%;" height="100%">
  <tr valign="top" height="100%">
    <td class="leftblock" height="100%">
    <table cellspacing="0" cellpadding="0" id="regionleft" height="100%" style="margin-top:15px;" width="212px">
      <tr height="11px">
        <td class="bar-top-left" height="11px"> 
        <td class="bar-top-line" height="11px"> 
        <td class="bar-top-right" height="11px">
      <tr valign="top" height="100%">
        <td class="bar-left-line">
        <td height="100%"><?php print $left ?>
        <td class="bar-right-line">
      <tr height="11px">
        <td class="bar-bottom-left" height="11px"><img src="/themes/energosoyuz/bars/bar-bottom-left.bmp" height="11px" width="11px">
        <td class="bar-bottom-line" height="11px"><img src="/themes/energosoyuz/bars/bar-line-bottom-full.bmp" height="11px" width="190px">
        <td class="bar-bottom-right" height="11px"><img src="/themes/energosoyuz/bars/bar-bottom-right.bmp" height="11px" width="11px">
    </table>

    <td class="rightblock" height="100%">
      <div id="main" class="contentarea">
        <?php print $breadcrumb ?>
        <!-- img width="700" height="4" src="/sites/default/files/Polosa.gif"/ style="padding: 0px; margin: 0px;" -->
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php if ($show_messages) { print $messages; } ?>
        <?php print $help ?>
        <?php print $content; ?>
        <?php print $feed_icons; ?>
      </div>
  </table>
 <tr>
  <td align="center">
  <table width="1005px" cellspacing="0" cellpadding="0" class="bottom"  id="regionmain">
    <tr height="60px;" valign="bottom">
      <td class="bottom">
      <div align="right">
        <div style="width:400px; color:#FFF; font: normal 8pt arial; padding: 3px;" align="right">
           Copyright (c) 2009-<?php $a = getdate(); echo $a['year']; ?> ЗАО «НПФ «ЭНЕРГОСОЮЗ»
        </div>
      </div>
  </table>

  
  <div id="footer">
    <?php print $footer_message ?>
    <?php print $footer ?>
  </div>

</table>

        <td class="mainbar-right-line">
      <tr>
        <td class="mainbar-bottom-left"> 
        <td class="mainbar-bottom-line"> 
        

<!--Rating@Mail.ru counter-->
<script language="javascript"><!--
d=document;var a='';a+=';r='+escape(d.referrer);js=10;//--></script>
<script language="javascript1.1"><!--
a+=';j='+navigator.javaEnabled();js=11;//--></script>
<script language="javascript1.2"><!--
s=screen;a+=';s='+s.width+'*'+s.height;
a+=';d='+(s.colorDepth?s.colorDepth:s.pixelDepth);js=12;//--></script>
<script language="javascript1.3"><!--
js=13;//--></script><script language="javascript" type="text/javascript"><!--
d.write('<a href="http://top.mail.ru/jump?from=1821056" target="_top">'+
'<img src="http://d9.cc.bb.a1.top.mail.ru/counter?id=1821056;t=210;js='+js+
a+';rand='+Math.random()+'" alt="Рейтинг@Mail.ru" border="0" '+
'height="31" width="88"><\/a>');if(11<js)d.write('<'+'!-- ');//--></script>
<noscript><a target="_top" href="http://top.mail.ru/jump?from=1821056">
<img src="http://d9.cc.bb.a1.top.mail.ru/counter?js=na;id=1821056;t=210" 
height="31" width="88" border="0" alt="Рейтинг@Mail.ru"></a></noscript>
<script language="javascript" type="text/javascript"><!--
if(11<js)d.write('--'+'>');//--></script>
<!--// Rating@Mail.ru counter-->

<!-- HotLog -->
<script type="text/javascript" language="javascript">
hotlog_js="1.0"; hotlog_r=""+Math.random()+"&s=2127439&im=605&r="+
escape(document.referrer)+"&pg="+escape(window.location.href);
</script>
<script type="text/javascript" language="javascript1.1">
hotlog_js="1.1"; hotlog_r+="&j="+(navigator.javaEnabled()?"Y":"N");
</script>
<script type="text/javascript" language="javascript1.2">
hotlog_js="1.2"; hotlog_r+="&wh="+screen.width+"x"+screen.height+"&px="+
(((navigator.appName.substring(0,3)=="Mic"))?screen.colorDepth:screen.pixelDepth);
</script>
<script type="text/javascript" language="javascript1.3">
hotlog_js="1.3";
</script>
<script type="text/javascript" language="javascript">
hotlog_r+="&js="+hotlog_js;
document.write('<a href="http://click.hotlog.ru/?2127439" target="_blank"><img '+
'src="http://hit37.hotlog.ru/cgi-bin/hotlog/count?'+
hotlog_r+'" border="0" width="88" height="31" alt="HotLog"><\/a>');
</script>
<noscript>
<a href="http://click.hotlog.ru/?2127439" target="_blank"><img
src="http://hit37.hotlog.ru/cgi-bin/hotlog/count?s=2127439&im=605" border="0"
width="88" height="31" alt="HotLog"></a>
</noscript>
<!-- /HotLog -->







<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=22285184&amp;from=informer"
target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/22285184/3_0_FFFFFFFF_EFEFEFFF_0_pageviews"
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="22285184" data-lang="ru" /></a>
<!-- /Yandex.Metrika informer -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22285184 = new Ya.Metrika({
                    id:22285184,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22285184" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
        
        <td class="mainbar-bottom-right"> 
    </table>

<?php print $closure ?>
</body>
</html>
