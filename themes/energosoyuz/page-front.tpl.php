﻿<?php
// $Id: page.tpl.php,v 1.28 2008/01/24 09:42:52 goba Exp $
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<?php $base_path = base_path() . path_to_theme(); ?>

<body>
<a name="toptop"></a>

    <table cellspacing="0" cellpadding="0" width="1100" align="center">
      <tr valign="top">
        <td class="mainbar-left-line">
        <td>


<table id="indexpage" style="max-width:1000px; width:1000px;" align="center" cellspacing="0" cellpadding="0">

 <tr style="height:180px;">
  <td style="padding: 0px; margin: 0px;">
    <img src="<?php print $base_path?>/logo.jpg" border="0" align="left" usemap="#karta1">
    <map name="karta1">
      <area href="/" shape="rect" coords="0,0,430,130">
    </map>


 <tr style="height: 25px;">
  <td style="border-top: 0px;">
    <table id="header-region" align="left" style="top: 0px; width: 100%;" cellspacing="0" cellpadding="0">
    <tr style="top: 0px;">
      <td style="top: 0px;"><?php print $header ?>
    </table>
 <tr>
  <td>
  
  <table style="width: 100%;" height="100%">
  <tr valign="top" height="100%">
    <td class="leftblock" height="600px">
    <table cellspacing="0" cellpadding="0" id="regionleft" height="100%">
      <tr>
        <td class="bar-top-left"> 
        <td class="bar-top-line"> 
        <td class="bar-top-right"> 
      <tr valign="top">
        <td class="bar-left-line">
        <td><?php print $left ?>
        <td class="bar-right-line">
      <tr height="11px">
        <td class="bar-bottom-left" height="11px"><img src="/themes/energosoyuz/bars/bar-bottom-left.bmp">
        <td class="bar-bottom-line" height="11px"><img src="/themes/energosoyuz/bars/bar-line-bottom-full.bmp">
        <td class="bar-bottom-right" height="11px"><img src="/themes/energosoyuz/bars/bar-bottom-right.bmp">
    </table>

    <td class="rightblock">
      <div id="main">
<table width="540px" align="center"><tr><td class="front-mainlink"><a href="/content/О-компании" class="front-mainlink" style="padding-left: 5px;">ЗАО «Научно-производственная фирма «ЭНЕРГОСОЮЗ» <p style="padding-left:300px;"> 25 лет на рынке автоматизации </p></a><tr><td class="front-toptext">
<p style="padding:5px;">
  <a href="/content/основная-выпускаемая-продукция"><ins>Разрабатывает, производит и внедряет</ins></a> оборудование для автоматизации объектов электроэнергетики, а также проводит 
  <a href="/content/энергетические-обследования"><ins>энергетические обследования</ins></a> энергообъектов и промышленных предприятий.
</p>
</table>

<br>

<table width="540px" bgcolor="#ffffff" align="center">
<tr valign="top" height="115px">
  <td width="270px">
  <table width="270px" class="noborder"><tr><td class="front-mainlink"><a href="/content/АСУ-ТП" class="front-mainlink" style="padding-left: 5px;">АСУ ТП</a><tr><td class="front-doplink">
<img src="/themes/energosoyuz/front/1.gif" align="left" style="margin: 5px;">
Комплексное решение построения АСУ ТП электрической части объекта (станции, подстанции, предприятия).
  </table>
  <td width="270px">
  <table width="270px" class="noborder"><tr><td class="front-mainlink"><a href="/content/Система-регистрации-аварийных-событий" class="front-mainlink" style="padding-left: 5px;"> Регистрация аварийных событий</a><tr><td class="front-doplink">
<img src="/themes/energosoyuz/front/4.gif" align="left" style="margin: 5px;">
Цифровое осциллографирование аварийных процессов и широкие возможности для анализа  осциллограмм.

  </table>

<tr valign="top" height="115px">
  <td width="270px">
  <table width="270px" class="noborder"><tr><td class="front-mainlink"><a href="/content/системы-телемеханики-сспи-соти-ассо-асду-э" class="front-mainlink" style="padding-left: 5px;"> Телемеханика</a><tr><td class="front-doplink">
<img src="/themes/energosoyuz/front/6.gif" align="left" style="margin: 5px;">
Системы телемеханики (ТМ), АСДУ, ССПИ, обмена технологической информацией с Системным оператором.

  </table>

  <td width="270px">
  <table width="270px" class="noborder"><tr><td class="front-mainlink"><a href="/content/контроль-и-диагностика-оборудования" class="front-mainlink" style="padding-left: 5px;">
  Контроль и диагностика</a><br><a href="/content/контроль-и-диагностика-оборудования" class="front-mainlink" style="padding-left: 5px;">технологического оборудования</a><tr><td class="front-doplink">
<img src="/themes/energosoyuz/front/3.gif" align="left" style="margin: 5px;">
Автоматизированные системы контроля и диагностики технологических параметров генераторов и масляных трансформаторов.
  </table>

<tr valign="top"  height="115px">
  <td width="270px">
  <table width="270px" class="noborder"><tr><td class="front-mainlink"><a href="/content/Энергетические-обследования" class="front-mainlink" style="padding-left: 5px;"> Энергообследование</a><tr><td class="front-doplink">
<img src="/themes/energosoyuz/front/5.gif" align="left" style="margin: 5px;">
Энергетические обследования и анализ качества электрической энергии на промышленных предприятиях и в энергоснабжающих организациях.

  </table>

  <td width="270px">
  <table width="270px" class="noborder"><tr height="100%"><td class="front-mainlink"><a href="/content/Система-противоаварийной-автоматики" class="front-mainlink" style="padding-left: 5px;"> Противоаварийная автоматика</a><tr height="100%"><td class="front-doplink" height="100%">
<img src="/themes/energosoyuz/front/2.gif" align="left" style="margin: 5px;">
Контроль режимов работы электрической сети с реализацией различных алгоритмов работы противоаварийной автоматики энергосистем.
  </table>

</table>


        
        <?php print $feed_icons; ?>
      </div>

    <!--td style="width: 200px;" --><!-- ?php print $right ? -->
    
    <td class="newsblock" height="600px">
    <table cellspacing="0" cellpadding="0" id="regionright">
      <tr>
        <td class="bar-top-left"> 
        <td class="bar-top-line"> 
        <td class="bar-top-right"> 
      <tr valign="top">
        <td class="bar-left-line">
        <td><?php print $right ?>
        <td class="bar-right-line">
      <tr>
        <td class="bar-bottom-left"> 
        <td class="bar-bottom-line"> 
        <td class="bar-bottom-right"> 
    </table>

    
  </table>
 <tr>
  <td align="center">
  <table width="1005px" cellspacing="0" cellpadding="0" class="bottom">
    <tr height="60px;" valign="bottom">
      <td class="bottom">
      <div align="right">
        <div style="width:400px; color:#FFF; font: normal 8pt arial; padding: 3px;" align="right">
           Copyright (c) 2009-<?php $a = getdate(); echo $a['year']; ?> ЗАО «НПФ «ЭНЕРГОСОЮЗ»
        </div>
      </div>
  </table>

  <div id="footer">
    <?php print $footer_message ?>
    <?php print $footer ?>
  </div>

</table>

        <td class="mainbar-right-line">
      <tr>
        <td class="mainbar-bottom-left"> 
        <td class="mainbar-bottom-line">

<!--Rating@Mail.ru counter-->
<script language="javascript"><!--
d=document;var a='';a+=';r='+escape(d.referrer);js=10;//--></script>
<script language="javascript1.1"><!--
a+=';j='+navigator.javaEnabled();js=11;//--></script>
<script language="javascript1.2"><!--
s=screen;a+=';s='+s.width+'*'+s.height;
a+=';d='+(s.colorDepth?s.colorDepth:s.pixelDepth);js=12;//--></script>
<script language="javascript1.3"><!--
js=13;//--></script><script language="javascript" type="text/javascript"><!--
d.write('<a href="http://top.mail.ru/jump?from=1821056" target="_top">'+
'<img src="http://d9.cc.bb.a1.top.mail.ru/counter?id=1821056;t=210;js='+js+
a+';rand='+Math.random()+'" alt="Рейтинг@Mail.ru" border="0" '+
'height="31" width="88"><\/a>');if(11<js)d.write('<'+'!-- ');//--></script>
<noscript><a target="_top" href="http://top.mail.ru/jump?from=1821056">
<img src="http://d9.cc.bb.a1.top.mail.ru/counter?js=na;id=1821056;t=210" 
height="31" width="88" border="0" alt="Рейтинг@Mail.ru"></a></noscript>
<script language="javascript" type="text/javascript"><!--
if(11<js)d.write('--'+'>');//--></script>
<!--// Rating@Mail.ru counter-->

<!-- HotLog -->
<script type="text/javascript" language="javascript">
hotlog_js="1.0"; hotlog_r=""+Math.random()+"&s=2127439&im=605&r="+
escape(document.referrer)+"&pg="+escape(window.location.href);
</script>
<script type="text/javascript" language="javascript1.1">
hotlog_js="1.1"; hotlog_r+="&j="+(navigator.javaEnabled()?"Y":"N");
</script>
<script type="text/javascript" language="javascript1.2">
hotlog_js="1.2"; hotlog_r+="&wh="+screen.width+"x"+screen.height+"&px="+
(((navigator.appName.substring(0,3)=="Mic"))?screen.colorDepth:screen.pixelDepth);
</script>
<script type="text/javascript" language="javascript1.3">
hotlog_js="1.3";
</script>
<script type="text/javascript" language="javascript">
hotlog_r+="&js="+hotlog_js;
document.write('<a href="http://click.hotlog.ru/?2127439" target="_blank"><img '+
'src="http://hit37.hotlog.ru/cgi-bin/hotlog/count?'+
hotlog_r+'" border="0" width="88" height="31" alt="HotLog"><\/a>');
</script>
<noscript>
<a href="http://click.hotlog.ru/?2127439" target="_blank"><img
src="http://hit37.hotlog.ru/cgi-bin/hotlog/count?s=2127439&im=605" border="0"
width="88" height="31" alt="HotLog"></a>
</noscript>
<!-- /HotLog -->
<!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=22285184&amp;from=informer"
target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/22285184/3_0_FFFFFFFF_EFEFEFFF_0_pageviews"
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="22285184" data-lang="ru" /></a>
<!-- /Yandex.Metrika informer -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22285184 = new Ya.Metrika({
                    id:22285184,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22285184" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->